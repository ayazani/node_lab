const http = require("http");
const port = 8080;
const urlParser = require("url");
const fs = require("fs");
let names = [];

const requestHandler = (req, res) => {
    let message;
    if (fs.existsSync("db.json")){
        names = JSON.parse(fs.readFileSync("db.json", "utf-8"));
    }
    const {headers, method, url} = req;
    if (method === "POST" && headers['iknowyoursecret'] === 'TheOwlsAreNotWhatTheySeem'){
        const isName = urlParser.parse(url, true).query.includes("name");
        if(isName){
            const name = urlParser.parse(url).query.split("=")[1];
            names.push({"name": name, "ip": req.connection.remoteAddress})
            fs.writeFile('db.json', JSON.stringify(names), (err) => {
                if (err) {
                    throw err;
                }
            });
            if (names) {
                names.forEach( elem =>{
                    message += `, ${elem}`
                })
                res.end(`Hello${message}`)
            }
            res.end(`Hello, ${name}`)
        }
        res.end();
    }
    res.end("BB")
}

const server = http.createServer(requestHandler);

server.listen(port, (err) => {
    if (err){
        throw err;
    }
    console.log(`Server is listening to port ${port}`)
});
