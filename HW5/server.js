const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const passport = require("passport");
const jwt = require("jsonwebtoken");

const LocalStrategy = require("passport-local");
const BearerStrategy = require("passport-http-bearer");

const port = 8080;
const app = express();

mongoose.connect('mongodb://localhost:27017/mongodb');
const UserSchema = mongoose.Schema({
    username: String,
    password: String,
    jwt: String,
});
const User = mongoose.model('Users', UserSchema);

const localStrategy = new LocalStrategy(
    {username: 'username', password: 'password'},
    (username, password, done) => {
        User.findOne({username: username})
            .exec()
            .then((found) => {
                found ? done(null, false) : done (null, found)
            })
    }
);

const bearerStrategy = new BearerStrategy((token, done) => {
    User.findOne({token: token})
        .exec()
        .then((found) => {
            found ? done(null, false) : done (null, found)
        })
});

passport.serializeUser((user, done) => {
    const accessToken = jwt.sign({username: user.name}, process.env.SECRET_TOKEN, {expiresIn: '48h'});
    User.updateOne(
        {username: user.name},
        {jwt: accessToken},
        (err, updatedUser) => {
            done(null, updatedUser);
        }
    );
});
passport.deserializeUser((user, done) => {
    done(null, user)
});

app.use(bodyParser.json());
passport.use('local', localStrategy);
passport.use('bearer', bearerStrategy);
app.use(passport.initialize());

app.post(
    "/token",
    passport.authenticate("local", {
        successRedirect: "/success",
        failureRedirect: "/failure",
    })
);

app.get(
    "/",
    passport.authenticate("bearer", { session: false }),
    (req, res) => {
        console.log(
            `Your username is ${req.user.username}, and your jwt is ${req.user.jwt}`
        );
        res.end();
    }
);

app.listen(port, (err) => {
    if (err) {
        throw err;
    }
    User.find({}, (err, base) => {
        if (err) throw err;
        console.log("They've been there recently: ", base.map(user => user.username).join(', '));
    })
    console.log(`Server's port is ${port}`);
});
