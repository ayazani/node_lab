const express = require("express");
const fs = require("fs");
const bodyParser = require("body-parser");
const app = express();
const dbName = "db.json";

const list = [];
const port = 8080
let db = [];
if (fs.existsSync(dbName)) {
    db = JSON.parse(fs.readFileSync(dbName, "utf8"));
}

app.use(bodyParser.json());

app.use("/", (req, res, next) => {
    if (req.method === "GET") {
        res.end("Try using POST");
    }
    next();
});
app.post("/", (req, res) => {
    const name = req.query.name;
    const headers = req.headers;
    const secret = headers["iknowyoursecret"]

    if (!name) {
        res.end("Wrong name");
    }
    if (!secret || secret !== "TheOwlsAreNotWhatTheySeem"){
        res.end("Wrong header");
    }

    const ip = req.ip;
    const users = { name, ip };
    list.push(users);

    fs.writeFile(db, list.toString(), {encoding: 'utf-8'}, err => {
        if (err) {
            throw err;
        }
    });

    let msg = 'Hello';
    list.forEach(elem => {
        msg += `, ${elem.name}`
    })
    res.end(msg);
});

app.listen(port, (err) => {
    if (err) {
        throw err;
    }
    console.log(`Server's port is ${port}`);
});
