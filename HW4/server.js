const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const mongoose = require("mongoose");

const port = 8080;
let names = [];

mongoose.connect('mongodb://localhost:27017/mongodb');
const UserSchema = mongoose.Schema({name: String});
const User = mongoose.model('Users', UserSchema);

app.use(bodyParser.json());

app.use("/", (req, res, next) => {
    if (req.method === "GET") {
        res.end("Try using POST");
    }
    next();
});
app.post("/", (req, res) => {
    const name = req.query.name;
    const headers = req.headers;
    const secret = headers["iknowyoursecret"]

    if (!name) {
        res.end("Wrong name");
    }
    if (!secret || secret !== "TheOwlsAreNotWhatTheySeem"){
        res.end("Wrong header");
    }

    const user = new User({name});
    user.save((error, newUser) => {
        if (error) {
            throw error;
        }
        const {name} = newUser;
        names.push(name);
        console.log(`${name} has joined`);
        res.end(`Hello ${name}!`);
    });
});

app.listen(port, (err) => {
    if (err) {
        throw err;
    }
    User.find({}, (err, base) => {
        if (err) throw err;
        console.log("They've been there recently: ", base.map(user => user.name).join(', '));
    })
    console.log(`Server's port is ${port}`);
});
