const http = require('http');

const params = {
    method : "POST",
    hostname: "localhost",
    port: 8080,
    path: "/?name=David_Blane",
    headers: {
        "IKnowYourSecret" : "TheOwlsAreNotWhatTheySeem",
    }
}

const req = http.request(params, res => {
    console.log(`statusCode: ${res.statusCode}`)
    res.on('data', d => {
        process.stdout.write(d);
    })
})
req.on('error', error => {
    if (error) throw  error;
});
req.end();
