const http = require("http");
const port = 8080;

const requestHandler = (req, res) => {
   const {headers, method} = req;
   switch (method) {
       case "GET":
           res.end("You send GET");
           break;
       case "POST":
           if (JSON.stringify(headers)) res.end("You send WhatWillSaveTheWorld - Love")
           else res.end("You send POST without headers");
           break;
   }
}
const server = http.createServer(requestHandler);

server.listen(port, (err) => {
    if (err){
        throw err;
    }
    console.log(`Server is listening to port ${port}`)
});
